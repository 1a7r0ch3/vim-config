## Some vim syntax and plugin files

Check `optplugin/programming.vim` for a comprehensive code source commenting utility.  

  
Check `ftdetect/blackboard.vim`, `ftplugin/blackboard.vim` and `syntax/blackboard.vim` for use with the [Remote Lecture](https://gitlab.com/1a7r0ch3/remote-lecture) software.  

### License
This software is under the GPLv3 license.
