"" Vim syntax file
" Language:	Matlab and GNU Octave (additions)
" License:  GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

syn keyword matlabOperator		rng randi randn poissrnd exprnd gamrnd length numel any all cat strvcat cell struct qr cumsum cumprod str2num num2str double single char mat2cell cell2mat struct2cell cell2struct mat2str real imag conj log2 linspace logspace varargin varargout nargin nargout save kron randperm and or xor not find sort sortrows shiftdim logical squeeze var repmat norm strfind sparse gamma full eq lt gt plus minus times mtimes rdivide ldivide cellfun arrayfun bsxfun inv pinv diag padarray ndims mod conv conv2 convn imfilter xcorr xcorr2 filter filter2 fft fft2 fftn ifft ifft2 ifftn fftshift ifftshift dct idct dct2 idct2 exist parfor semilogx semilogy loglog meshgrid minmax ind2sub sub2ind continue break codistributed distributed getLocalPart spmd codistributor1d codistributor2dbc gather dfeval class fseek ftell feof lcm fields complex circshift bitand bitor bitxor bitshift lower upper strrep fileattrib str2double dec2bin bin2dec dec2hex hex2dec str2func ancestor dlmread fix uigetfile listdlg findobj optimset lsqcurvefit cosd sind acosd asind median range fminunc fmincon svd svds fminsearch fmincon input inputdlg unique permute ipermute strcat factorial strsplit delete det nonzeros nnz avifile addframe VideoWriter writeVideo getframe cast isa window cond rcond trace accumarray rescale dlmwrite accumdim repelem repelems

syn match matlabOperator		"\<is[^w]\w\+"
syn match matlabOperator 		"u\=int\d\+"
syn match matlabOperator		"strn\=cmpi\="
syn match matlabOperator		"regexp\w*"

syn keyword matlabNumber		realmin realmax eps inf Inf nan NaN pi true false

syn keyword matlabFunction		warning system rethrow lasterror lasterr assert
syn match matlabFunction		"@"

syn keyword matlabImplicit		plot plot3 clf cla gca gcf xlabel ylabel zlabel title legend hold on off axis tight square equal vis3d figure fscanf fprintf sprintf open read write close clear disp tic toc cputime set get subplot pause drawnow boxplot which what who whos print camlight shading interp image imagesc hist path addpath box jet hot addpath genpath doc help cd alpha colormap imshow imread imwrite fopen fclose fread fwrite load fullfile matlabpool openpool labindex numlabs fileparts mkdir setenv getenv exit pwd guidata axes rectangle line dir daspect sphere surf trisurf mesh surfl surfc colorbar datestr now jet HSV hot cool spring summer autumn winter gray bone copper pink lines colorcube prism flag text textscan patch

syn match matlabDelimiter		"[({})]"

syn match matlabArithmeticOperator	"[\.,;:]"
syn match matlabRelationalOperator	"[\~=]="

syn match matlabComment			"\.\.\..*$"
