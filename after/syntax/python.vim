" Vim syntax file
" Language:     Python (additions)
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

syn match pythonOperator "\(+\|-\|=\|*\|/\|\.\|,\|;\|:\|<\|>\|!\|%\|\~\|&\|\^\||\)"

syn keyword pythonStatement self
