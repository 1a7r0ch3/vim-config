" Vim syntax file
" Language:     R (additions)
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

" color appropriately blocks commented with if (FALSE){ ... }
" for some reasons, match only if start with special character
syn match rBckComDelim "\(\nif\s*(FALSE){\|}\)" contained contains=rStatement
syn region rComment start="\nif\s*(FALSE){" end="}" contains=rBckComDelim keepend
