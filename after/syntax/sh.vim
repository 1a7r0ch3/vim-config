" Vim syntax file
" Language:     Shell (additions)
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

" color appropriately blocks commented with <<COMMENT ... COMMENT
syn region shComment matchgroup=shOperator start="<<COMMENT" end="COMMENT"
