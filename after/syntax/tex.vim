" Vim syntax file
" Language:     TeX (additions)
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

" Better handling of texZone in listings package
" https://latex.org/forum/viewtopic.php?t=32529
"
" Separate lines used for verb` and verb# so that the end conditions {{{1
" will appropriately terminate.
" If g:tex_verbspell exists, then verbatim texZones will permit spellchecking there.
" if s:tex_fast =~# 'v'
  if exists("g:tex_verbspell") && g:tex_verbspell
   syn region texZone		start="\\begin{lstlisting}"		end="\\end{lstlisting}\|%stopzone\>"	contains=@Spell
   syn region texZone		start="\\begin{lstconsole}"		end="\\end{lstconsole}\|%stopzone\>"	contains=@Spell
  else
   syn region texZone		start="\\begin{lstlisting}"		end="\\end{lstlisting}\|%stopzone\>"
   syn region texZone		start="\\begin{lstconsole}"		end="\\end{lstconsole}\|%stopzone\>"
  endif
" endif


" color appropriately blocks commented with \if 0 ... \fi
syn match texBckComDelim "\(\\if\s*0\|\\fi\)" contained contains=texStatement
syn region texComment start="\\if\s*0" end="\\fi" contains=texBckComDelim keepend
syn match texBckComDelim "\(\\iffalse\|\\fi\)" contained contains=texStatement
syn region texComment start="\\iffalse" end="\\fi" contains=texBckComDelim keepend
