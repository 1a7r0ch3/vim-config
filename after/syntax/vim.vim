" Vim syntax file
" Language:     Vim (additions)
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

" color appropriately blocks commented with if 0 ... endif
syn match vimBckComDelim "\(if\s*0\|endif\)" contained contains=vimCommand,vimNumber
syn region vimComment start="if\s*0" end="endif" contains=vimBckComDelim keepend
syn match vimBckComDelim "\(if\s*v:false\|endif\)" contained contains=vimCommand,vimNumber
syn region vimComment start="if\s*v:false" end="endif" contains=vimBckComDelim keepend
