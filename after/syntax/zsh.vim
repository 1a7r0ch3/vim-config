" Vim syntax file
" Language:     Zsh (additions)
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

" color appropriately blocks commented with <<COMMENT ... COMMENT
syn region zshComment matchgroup=zshOperator start="<<COMMENT" end="COMMENT"
