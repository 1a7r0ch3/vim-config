" Vim filetype plugin file
" Language:	    C
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

" override the default cpp
au BufRead,BufNewFile *.h setlocal filetype=c
