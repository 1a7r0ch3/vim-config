" Vim filetype plugin file
" Language:	    CSV (and variants)
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

au BufRead,BufNewFile *.csv,*.dat,*.tsv,*.tab setlocal filetype=csv
