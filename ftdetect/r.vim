" Vim filetype plugin file
" Language:	    R
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

au BufRead,BufNewFile *.Rprofile setlocal filetype=r
