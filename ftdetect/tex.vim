" Vim filetype plugin file
" Language:	    TeX (and variants)
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

au BufRead,BufNewFile *.tex,*.ltx,*.tikz,*.pst,*.bib,*.sty,*.cls,*.bst
    \ setlocal filetype=tex
