" Vim filetype plugin file
" Language:	    Textboard (to be used with Remote Lecture software)
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

au BufRead,BufNewFile *.tbd,*.textboard setlocal filetype=textboard
