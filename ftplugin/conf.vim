" Vim filetype plugin file
" Language:	    Conf file
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

runtime optplugin/programming.vim

call MapKeyToTagsAndTitles('#', '#', ' ', '')
