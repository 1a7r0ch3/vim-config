" Vim filetype plugin file
" Language:	    C++
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

runtime optplugin/programming.vim

let b:com_lin_beg='//'
let b:com_lin_sep=' '
let b:com_lin_end=''
let b:com_bck_beg='/*'
let b:com_bck_sep=' '
let b:com_bck_end='*/'

call MapKeyToTagsAndTitles('*', '/*', ' ', '*/')

call MapKeyToTags('#', '#if 0', '\n', '#endif')

" the system wide cpp.vim loads local c.vim, overriding the F1 keys
" setting the following variable prevent this
let b:did_ftplugin = 1
