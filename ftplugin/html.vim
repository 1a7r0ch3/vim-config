" Vim filetype plugin file
" Language:	    HTML
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

" cpp-style comments for javascript and css
runtime ftplugin/cpp.vim

" xml-style comments and tags
runtime ftplugin/xml.vim

setlocal textwidth=0

" common html tags
call MapKeyToTags('i', '<i>', '', '</i>')
call MapKeyToTags('b', '<b>', '', '</b>')
call MapKeyToTags('g', '<big>', '', '</big>')
call MapKeyToTags('s', '<s>', '', '</s>')
call MapKeyToTags('u', '<u>', '', '</u>')
call MapKeyToTags('p', '<p>', '', '</p>')
call MapKeyToTags('h', '<a href="">', '', '</a>')
call MapKeyToTags('r', '', '', '</br>')

" quick preview
command! View :!nohup qutebrowser % >
    \ $HOME/.local/share/qutebrowser/nohup.out&

" HTML sans unicode
command Accenthtml
    \ :%s/à/&agrave;/eg | :%s/â/&acirc;/eg | :%s/ä/&auml;/eg |
    \ :%s/é/&eacute;/eg | :%s/è/&egrave;/eg | :%s/ê/&ecirc;/eg |
    \ :%s/ë/&euml;/eg | :%s/î/&icirc;/eg | :%s/ï/&iuml;/eg |
    \ :%s/ô/&ocirc;/eg | :%s/ö/&ouml;/eg | :%s/ù/&ugrave;/eg |
    \ :%s/û/&ucirc;/eg | :%s/ü/&uuml;/eg | :%s/À/&Agrave;/eg |
    \ :%s/Â/&Acirc;/eg | :%s/Ä/&Auml;/eg | :%s/É/&Eacute;/eg |
    \ :%s/È/&Egrave;/eg | :%s/Ê/&Ecirc;/eg | :%s/Ë/&Euml;/eg |
    \ :%s/Î/&Icirc;/eg | :%s/Ï/&Iuml;/eg | :%s/Ô/&Ocirc;/eg | :%s/Ö/&Ouml;/eg |
    \ :%s/Ù/&Ugrave;/eg | :%s/Û/&Ucirc;/eg | :%s/Ü/&Uuml;/eg

command Invaccenthtml
    \ :%s/&agrave;/à/eg | :%s/&acirc;/â/eg | :%s/&auml;/ä/eg |
    \ :%s/&eacute;/é/eg | :%s/&egrave;/è/eg | :%s/&ecirc;/ê/eg |
    \ :%s/&euml;/ë/eg | :%s/&icirc;/î/eg | :%s/&iuml;/ï/eg |
    \ :%s/&ocirc;/ô/eg | :%s/&ouml;/ö/eg | :%s/&ugrave;/ù/eg |
    \ :%s/&ucirc;/û/eg | :%s/&uuml;/ü/eg | :%s/&Agrave;/À/eg |
    \ :%s/&Acirc;/Â/eg | :%s/&Auml;/Ä/eg | :%s/&Eacute;/É/eg |
    \ :%s/&Egrave;/È/eg | :%s/&Ecirc;/Ê/eg | :%s/&Euml;/Ë/eg |
    \ :%s/&Icirc;/Î/eg | :%s/&Iuml;/Ï/eg | :%s/&Ocirc;/Ô/eg | :%s/&Ouml;/Ö/eg |
    \ :%s/&Ugrave;/Ù/eg | :%s/&Ucirc;/Û/eg | :%s/&Uuml;/Ü/eg

