" Vim filetype plugin file
" Language:	    mail
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

runtime ftplugin/text.vim

"" standard quoting with >
let b:com_lin_beg='>'
let b:com_lin_sep=' '
let b:com_lin_end=''
let b:com_bck_beg='>'
let b:com_bck_sep=' '
let b:com_bck_end=''
call MapKeyToTags('>', '>', '', '')
call MapKeyToTags("<Leader>>", '>', ' ', '')

" Open a read-only mutt to browse other mails
command Mutt :silent !$TERMCMD -e mutt -e 'set read_only'&

command TightQuotes :%s/^>[ >]\+/\=substitute(submatch(0), ' ', '', 'g').' '/e

" the system wide mail.vim set textwidth to 72 chars;
" let the mail client handle such stuff!
" setting the following variable prevent this
let b:did_ftplugin = 1
