" Vim filetype plugin file
" Language:	    Markdown
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

runtime ftplugin/text.vim
runtime ftplugin/html.vim

call MapKeyToTags('_', '_', '', '_')
call MapKeyToTags('<Leader>_', '__', '', '__')
call MapKeyToTags('*', '*', '', '*')
call MapKeyToTags('<Leader>*', '**', '', '**')
call MapKeyToTags('h', '[', '', ']()')
call MapKeyToTags('#', '#', ' ', '')
call MapKeyToTags('<Leader>#', '##', ' ', '')
call MapKeyToTags('<Leader><Leader>#', '###', ' ', '')

" underline with dash
noremap  <Leader><Leader><Leader>_ yyp:s/./-/g<CR>
inoremap <Leader><Leader><Leader>_ <ESC>yyp:s/./-/g<CR>o

" quick preview
noremap <buffer> <F5> :!pandoc -f markdown -t html
    \ -o %:r".html" %<CR>
command -buffer Viewhtml !nohup qutebrowser %:r".html" >
    \ $HOME/.local/share/qutebrowser/nohup.out&

" the system wide markdown.vim loads local html.vim, overriding many keys
" setting the following variable prevent this
let b:did_ftplugin = 1
