" Vim filetype plugin file
" Language:	    Matlab and GNU Octave
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

runtime optplugin/programming.vim

let b:com_lin_beg='%'
let b:com_lin_sep=' '
let b:com_lin_end=''
let b:com_bck_beg='%{'
let b:com_bck_sep='\n'
let b:com_bck_end='%}'

call MapKeyToTagsAndTitles('%', '%', ' ', '')

" matlab specifics
iab <buffer> print\ fprintf('... ');<ESC>2F'a
iab <buffer> printn\ fprintf('\n');<ESC>2F'a
iab <buffer> done\ fprintf('done.\n');
iab <buffer> vprint\ vprintf(verbose, '... ');<ESC>2F'a
iab <buffer> vprintn\ vprintf(verbose, '\n');<ESC>2F'a
iab <buffer> vdone\ vprintf(verbose, 'done.\n');"
