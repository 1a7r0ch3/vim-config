" Vim filetype plugin file
" Language:	    Pseudocode (french)
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

if exists('b:loaded_pseudo') && b:loaded_pseudo
  finish
endif
let b:loaded_pseudo = 1

runtime optplugin/programming.vim

let b:com_lin_beg='#'
let b:com_lin_sep=' '
let b:com_lin_end=''
let b:com_bck_beg='<<COMMENT'
let b:com_bck_sep='\n'
let b:com_bck_end='COMMENT'

call MapKeyToTags('#', '#', ' ', '')

setlocal number

inoreabbr <buffer> <=   ≤
inoreabbr <buffer> >=   ≥
inoreabbr <buffer> <-   ←
inoreabbr <buffer> ->   →
inoreabbr <buffer> =/   ≠
inoreabbr <buffer> 0/   ∅
inoreabbr <buffer> /\   ÷
inoreabbr <buffer> *\   ×
inoreabbr <buffer> -\   −
inoremap  <buffer> '    ‘’<ESC>i
inoremap  <buffer> "    “”<ESC>i
