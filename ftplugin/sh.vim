" Vim filetype plugin file
" Language:	    Shell
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

runtime optplugin/programming.vim

let b:com_lin_beg='#'
let b:com_lin_sep=' '
let b:com_lin_end=''
let b:com_bck_beg='<<COMMENT'
let b:com_bck_sep='\n'
let b:com_bck_end='COMMENT'

call MapKeyToTagsAndTitles('#', '#', ' ', '')

" script shell headers for zsh
iab shebang\ #!/bin/zsh<CR>

" Auto chmod +x on shell scripts
" autocmd VimLeave *.sh !chmod u+x % "nah, too dangerous
