" Vim filetype plugin file
" Language:	    (La)TeX
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

runtime optplugin/programming.vim

setlocal textwidth=0

let b:com_lin_beg='%'
let b:com_lin_sep=' '
let b:com_lin_end=''
let b:com_bck_beg='\iffalse'
let b:com_bck_sep='\n'
let b:com_bck_end='\fi'

call MapKeyToTags('%', '%', ' ', '')

" compilation
noremap <buffer> <F5> :!pdflatex %<CR>
noremap <buffer> <F6> :!latex %<CR>
noremap <buffer> <F7> :!dvips %:r".dvi"<CR>
noremap <buffer> <F8> :!ps2pdf -dALLOWPSTRANSPARENCY %:r".ps"<CR>
noremap <buffer> <F9> :!xelatex
    \ --output-driver="xdvipdfmx -i dvipdfmx-unsafe.cfg" -shell-escape %<CR>
noremap <buffer> <F10>
    \ :!$TERMCMD -e pdflatex -output-dir %:p:h -shell-escape %&<CR>

" do not attempt to compile style and data files
autocmd BufEnter *.sty,*.bib,*.tikz,*.pst,*.cls,*.bst
    \ exe "silent! unmap <buffer> <F5>" |
    \ exe "silent! unmap <buffer> <F6>" |
    \ exe "silent! unmap <buffer> <F7>" |
    \ exe "silent! unmap <buffer> <F8>" |
    \ exe "silent! unmap <buffer> <F9>"

" bib files formating entry
autocmd FileType bib noremap <F5>
    \ <CR>:s/^\s\+/\t/g<CR><CR>:s/\s*=\s*/\t= /g<CR>

" run bib
function Bibcomplete(A,L,P)
    return system('find . -maxdepth 1 -name "*.aux" -printf "%f\n"')
endfunction
autocmd FileType tex command! -nargs=? -complete=custom,Bibcomplete Bib
    \ if empty(<q-args>) |
    \   execute '!bibtex %:r".aux"' |
    \ else |
    \   execute '!bibtex <args>' |
    \ endif
autocmd FileType tex command! Bibunit :!bibunit.sh
autocmd FileType tex command! Openbib :edit %:p:h"/References.bib"

" latex output openings
command! -buffer Viewpdf :!nohup zathura %:r".pdf" >
    \ $HOME/.local/share/zathura/nohup.out&
command! -buffer Viewps :!nohup zathura %:r".ps" >
    \ $HOME/.local/share/zathura/nohup.out&
command! -buffer Viewdvi :!nohup zathura %:r".dvi" >
    \ $HOME/.local/share/zathura/nohup.out&

" font styles or math mode
call MapKeyToTags('e', '\emph{', '', '}')
call MapKeyToTags('i', '\textit{', '', '}')
call MapKeyToTags('b', '\textbf{', '', '}')
call MapKeyToTags('c', '\textsc{', '', '}')
call MapKeyToTags('t', '\texttt{', '', '}')
call MapKeyToTags('s', '\textsf{', '', '}')
call MapKeyToTags('a', '\alert{', '', '}')
call MapKeyToTags('$', '$', '', '$')
call MapKeyToTags('g', '\og', ' ', '\fg')
" call MapKeyToTags('B', '\begin{}', '\n', '\end{}')
" must be donne manually because only TagBlock() can interprete \n
noremap  <buffer> <Leader>B
    \ i\begin{}<CR><CR>\end{}<ESC>2k2li
imap     <buffer> <Leader>B <ESC><Leader>B
vnoremap <buffer> <Leader>B <ESC>:call
    \ TagBlock('\begin{}', '\n', '\end{}')<CR>
    \ j$i

"""  templates for figures, table, equation, graphes, slides, etc.

" floats
iab table\
    \ \begin{table}<CR>tabular\ \caption{}
    \ <CR>\label{tab.}<CR>\end{table}
iab tabular\
    \ \begin{tabular}{ccc}<CR>\toprule<CR>  &  &  \\<CR>\midrule
    \ <CR>  &  &  \\<CR>  &  &  \\<CR>\bottomrule<CR>\end{tabular}<CR>
iab figure\
    \ \begin{figure}<CR>\includegraphics[scale=1]{}
    \ <CR>\caption{}<CR>\label{fig.}<CR>\end{figure}
iab subfigure\
    \ \begin{figure}<CR>\ffigbox[\textwidth]{
    \ <CR>\begin{subfloatrow}[2]
    \     <CR>\ffigbox[\FBwidth]{%
    \         <CR>\includegraphics[width=.5\textwidth]{}
    \     <CR>}{\caption[]{}\label{sfig.}}
    \     <CR>\ffigbox[\FBwidth]{%
    \         <CR>\includegraphics[width=.5\textwidth]{}
    \     <CR>}{\caption[]{}\label{sfig.}}
    \ <CR>\end{subfloatrow}
    \ <CR>}{\caption[]{}\label{fig.}}<CR>\end{figure}

" equations
iab eqnarray\
    \ \begin{eqnarray}
    \ <CR> & = &  \label{eq.}\\
    \ <CR> & = &  \label{eq.}\\
    \ <CR> & = &  \label{eq.}
    \ <CR>\end{eqnarray}
iab align\
    \ %<CR>\begin{align}
    \ <CR> ={} &  \label{eq.}\\
    \ <CR> ={} &  \label{eq.}\\
    \ <CR> ={} &  \label{eq.}
    \ <CR>\end{align}<CR>%
iab align*\
    \ %<CR>\begin{align*}
    \ <CR> ={} &\\
    \ <CR> ={} &\\
    \ <CR> ={} &
    \ <CR>\end{align*}<CR>%
iab flalign\
    \ \begin{flalign}
    \ <CR> \label{eq.}\\
    \ <CR> \label{eq.}\\
    \ <CR> \label{eq.}
    \ <CR>\end{flalign}
iab equation\
    \ %<CR>\begin{equation}\label{eq.}<CR> = <CR>\end{equation}<CR>%
iab equation*\
    \ %<CR>\begin{equation*}<CR> = <CR>\end{equation*}<CR>%
iab gather\
    \ %<CR>\begin{gather}\label{eq.}<CR> \end{gather}<CR>%
iab split\
    \ %<CR>\begin{equation}\label{eq.}
    \  <CR>\begin{split}<CR>  \end{split}
    \ <CR>\end{equation}<CR>%
iab multline\
    \ %<CR>\begin{multline}\label{eq.}
    \ <CR>  & = \\<CR>
    \ <CR>\end{multline}<CR>%
iab multline*\
    \ %<CR>\begin{multline*}
    \ <CR> & = \\<CR>
    \ <CR>\end{multline*}<CR>%
iab multlined\
    \ %<CR>\setlength{\multlinegap}{0pt}
    \ <CR>\begin{equation}\label{eq.}
    \   <CR>\begin{multlined}[c][.92\textwidth]%for 2-digits eqn num
    \   <CR>\end{multlined}
    \ <CR>\end{equation}<CR>%
iab cases\ \begin{cases}    & \sifs   \\   & \stxts{otherwise}   \end{cases}

" math arrays
iab array\ \left\{ \begin{array}{rcl}   &   & \end{array}\right.
iab func\ \colon \:   \to   \colon \:   \mapsto   <CR>
iab funcarr\
    \ \colon \begin{array}{ccc} & \longrightarrow & \\
    \ <CR>& \longmapsto & \end{array}
iab vec\ $x=\left( \begin{array}{c}  1 \\ \vdots \\ 1 \end{array} \right)$
iab frac\ \frac{}{}<ESC>hhi
iab binom\ \binom{}{}<ESC>hhi

" document structure
iab sec\
    \     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \ <CR>%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \ <CR>%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \ <CR>\section{}<CR>\label{sec.}
iab ssec\
    \     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \ <CR>\subsection{}<CR>\label{ssec.}
iab sssec\ <TAB>\subsubsection{}<CR>\label{sssec.}
iab an\
    \     % % % % % % % % % % % % % % % % % % % % % % % % % %
    \ <CR>% % % % % % % % % % % % % % % % % % % % % % % % % %
    \ <CR>% % % % % % % % % % % % % % % % % % % % % % % % % %
    \ <CR>\section{}<CR>\label{an.}
iab san\
    \     % % % % % % % % % % % % % % % % % % % % % % % % % %
    \ <CR>\subsection{}<CR>\label{san.}

" lists
iab enum\
    \ \begin{enumerate}[label={\rm(H\roman{*})}, ref={\rm(H\roman{*})},
    \   series=name,%[resume*=name]
    \   <CR>topsep=0pt plus 1pt, partopsep=0pt plus 1pt,
    \   itemsep=0pt plus 1pt, parsep=0pt plus 1pt,
    \   labelindent=0pt plus 1pt, leftmargin=*]
    \   <CR>\setcounter{enumi}{-1}
    \   <CR>\item{\label{it.}}
    \   <CR>\item{\label{it.}}
    \   <CR>\item{\label{it.}}
    \ <CR>\end{enumerate}
iab item\
    \ \begin{itemize}[topsep=.5ex, itemsep=.5ex, wide=1em, labelsep=0pt,
    \   label=]
    \   <CR><TAB>\item<CR><TAB>\item<CR><TAB>\item
    \ <CR>\end{itemize}<ESC>3kf]i

" beamer structures
iab frame\
    \ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \ <CR>\begin{frame}<CR>\frametitle{}<CR>\framesubtitle{}
    \ <CR>\end{frame}<ESC>2kf}i
iab columns\
    \ \begin{columns}[T,onlytextwidth]
    \ <CR>\begin{column}{.5\textwidth}
    \ <CR><CR>\end{column}\begin{column}{.5\textwidth}
    \ <CR><CR>\end{column}
    \ <CR>\end{columns}<ESC>4ki
iab block\ \begin{block}{}<CR><CR>\end{block}<ESC>2k$i

" fill spaces
iab hfill\ \hspace*{\fill}
iab vfill\ \vspace*{\fill}

""" open documentations
command! Man :!$SHELL -c
    \ "cd ~/Library/Manuals/LaTeX && $TERMCMD"&
command! Pst :!$SHELL -c
    \ "cd ~/Library/Manuals/LaTeX/graphics/pstricks && $TERMCMD"&
command! Symbols :!zathura 
    \ ~/Library/Manuals/LaTeX/the_comprehensive_LaTeX_symbol_list.pdf&
command! Maths :!zathura 
    \ ~/Library/Manuals/LaTeX/science/mathmode.pdf&
command! Graphicx :!zathura 
    \ ~/Library/Manuals/LaTeX/graphics/graphicx.pdf&
command! Subfig :!zathura  
    \ ~/Library/Manuals/LaTeX/graphics/float/subfigure.pdf&
command! Pstricks :!zathura  
    \ ~/Library/Manuals/LaTeX/graphics/pstricks/pstricks.pdf&
command! Listings :!zathura 
    \ ~/Library/Manuals/LaTeX/science/listings.pdf&
command! Beamer :!zathura 
    \ ~/Library/Manuals/LaTeX/beamer.pdf&
command! Array :!zathura 
    \ ~/Library/Manuals/LaTeX/layout/array.pdf&
command! Fonts :!zathura 
    \ ~/Library/Manuals/LaTeX/fonts/LaTeX2e_font_selection.pdf&
command! Boxes :!zathura 
    \ ~/Library/Manuals/LaTeX/LaTeX2e.pdf&

" open custom style file
command! Sty :edit ~/Codes/LaTeX/templates/raguet.sty

" not using unicode encoding
command Accenttex
    \ :%s/à/\\`a/eg | :%s/â/\\^a/eg | :%s/ä/\\"a/eg | :%s/é/\\'e/eg |
    \ :%s/è/\\`e/eg | :%s/ê/\\^e/eg | :%s/ë/\\"e/eg | :%s/î/\\^i/eg |
    \ :%s/ï/\\"i/eg | :%s/ô/\\^o/eg | :%s/ö/\\"o/eg | :%s/ù/\\`u/eg |
    \ :%s/û/\\^u/eg | :%s/ü/\\"u/eg | :%s/À/\\`A/eg | :%s/Â/\\^A/eg |
    \ :%s/Ä/\\"A/eg | :%s/É/\\'E/eg | :%s/È/\\`E/eg | :%s/Ê/\\^E/eg |
    \ :%s/Ë/\\"E/eg | :%s/Î/\\^I/eg | :%s/Ï/\\"I/eg | :%s/Ô/\\^O/eg |
    \ :%s/Ö/\\"O/eg | :%s/Ù/\\`U/eg | :%s/Û/\\^U/eg | :%s/Ü/\\"U/eg
command Invaccenttex
    \ :%s/\\`a/à/eg | :%s/\\^a/â/eg | :%s/\\"a/ä/eg | :%s/\\'e/é/eg |
    \ :%s/\\`e/è/eg | :%s/\\^e/ê/eg | :%s/\\"e/ë/eg | :%s/\\^i/î/eg |
    \ :%s/\\"i/ï/eg | :%s/\\^o/ô/eg | :%s/\\"o/ö/eg | :%s/\\`u/ù/eg |
    \ :%s/\\^u/û/eg | :%s/\\"u/ü/eg | :%s/\\`A/À/eg | :%s/\\^A/Â/eg |
    \ :%s/\\"A/Ä/eg | :%s/\\'E/É/eg | :%s/\\`E/È/eg | :%s/\\^E/Ê/eg |
    \ :%s/\\"E/Ë/eg | :%s/\\^I/Î/eg | :%s/\\"I/Ï/eg | :%s/\\^O/Ô/eg |
    \ :%s/\\"O/Ö/eg | :%s/\\`U/Ù/eg | :%s/\\^U/Û/eg | :%s/\\"U/Ü/eg

" inclusive middle dot; for better spacing, users of [utf8]{inputenc}
" can use 
" \DeclareUnicodeCharacter{0183}{\kern.05em\textperiodcentered\kern.05em}
inoremap <Leader>. ·
