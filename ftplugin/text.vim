" Vim filetype plugin file
" Language:	    Plain text
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

runtime optplugin/programming.vim

set textwidth=0

" some unusual delimiters
call MapKeyToTags('`', '`', '', '`')
call MapKeyToTags("<Leader>'", '‘', '', '’')
call MapKeyToTags('<Leader>"', '“', '', '”')
" the following contain nonbreakable spaces
call MapKeyToTags('<Leader><', '«', ' ', '»')

" underline with dash
noremap  <Leader>_ yyp:s/./-/g<CR>
inoremap <Leader>_ <ESC>yyp:s/./-/g<CR>o

" en dash
inoremap <Leader>- –

" strikethrough with COMBINING LONG STROKE OVERLAY
inoremap <Leader><Leader>-  ̶
noremap <Leader><Leader>- :s/[^[:cntrl:]]/&̶/g<CR>
vnoremap <Leader><Leader>- <ESC><ESC>:s/\%V[^[:cntrl:]]/&̶/g<CR>

" underline with COMBINING LOW LINE
inoremap <Leader><Leader>_  ̲
noremap <Leader><Leader>_ :s/[^[:cntrl:]]/&̲/g<CR>
vnoremap <Leader><Leader>_ <ESC><ESC>:s/\%V[^[:cntrl:]]/&̲/g<CR>

" overline with COMBINING OVERLINE
inoremap <Leader><Leader>^  ̅
noremap <Leader><Leader>^ :s/[^[:cntrl:]]/&̅/g<CR>
vnoremap <Leader><Leader>^ <ESC><ESC>:s/\%V[^[:cntrl:]]/&̅/g<CR>

" middle dot
inoremap <Leader>. ·

" narrow nonbreakable space
inoremap <Leader><Space>  
inoremap <Leader>;  ;
inoremap <Leader>:  :
inoremap <Leader>!  !
inoremap <Leader>%  %
if v:false
inoreabbr ;\  ;
inoreabbr :\  :
inoreabbr !\  !
inoreabbr ?\  ?
inoreabbr %\  %
endif

" quick print and preview
noremap <buffer> <F5> :hardcopy > /tmp/%:r.ps<CR>
    \ :!ps2pdf /tmp/%:r.ps %:r.pdf<CR>
command! -buffer Viewpdf :!nohup zathura %:r".pdf" >
    \ $HOME/.local/share/zathura/nohup.out&
