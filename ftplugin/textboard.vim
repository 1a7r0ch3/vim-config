" Vim filetype plugin file
" Language:	    Textboard (to be used with Remote Lecture software)
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

runtime ftplugin/pseudo.vim
runtime ftplugin/html.vim
runtime ftplugin/text.vim

" undo features specific to pseudocode which might interfere with normal text
" or other languages
" TODO: use a system to toggle
setlocal nonumber
iunmap  <buffer> '
iunmap  <buffer> "
iunabbr <buffer> <=
iunabbr <buffer> >=
iunabbr <buffer> ->
iunabbr <buffer> <-

" jqMath mode
call MapKeyToTags('$', '$', '', '$')
call MapKeyToTags('<Leader>$', '$$', '', '$$')

"" HTML spaces
" line breaks
noremap  <buffer> <C-_>r o<br/><ESC>
noremap  <buffer> <C-_>R O<br/><ESC>
inoremap <buffer> <C-_>r <CR><br/>
" paragraphe mode white-space: pre-wrap;
call MapKeyToTags('w', '<pw>', '', '</pw>')

""" insert code 
"" inline - one leader
" pseudocode
call MapKeyToTags('p', '<code class="lang-pseudo">', '', '</code>')
" C
call MapKeyToTags('c', '<code class="lang-c">', '', '</code>')
"" paragraph - two leaders
" pseudocode
call MapKeyToTags('<Leader>p', '<pre><code class="lang-pseudo">', '', '</code></pre>')
" C
call MapKeyToTags('<Leader>c', '<pre><code class="lang-c">', '', '</code></pre>')

" special characters (greek letters, mathematics)
runtime optplugin/special_chars.vim

" use with Remote Lecture software
command LiveBroadcast
    \ execute "autocmd TextChanged,TextChangedI *.tbd,*.textboard silent write"
command StopBroadcast
    \ execute "autocmd! TextChanged,TextChangedI *.tbd,*.textboard"
