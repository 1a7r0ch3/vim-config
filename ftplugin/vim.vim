" Vim filetype plugin file
" Language:	    Vim
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

runtime optplugin/programming.vim

let b:com_lin_beg='"'
let b:com_lin_sep=' '
let b:com_lin_end=''
let b:com_bck_beg='if v:false'
let b:com_bck_sep='\n'
let b:com_bck_end='endif' 

call MapKeyToTagsAndTitles('"', '"', ' ', '')
