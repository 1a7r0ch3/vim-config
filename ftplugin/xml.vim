" Vim filetype plugin file
" Language:	    XML
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

runtime optplugin/programming.vim

" comment tags
let b:com_lin_beg='<!--'
let b:com_lin_sep=' '
let b:com_lin_end='-->'
let b:com_bck_beg='<!--'
let b:com_bck_sep=' '
let b:com_bck_end='-->'

call MapKeyToTagsAndTitles('>', '<!--', ' ', '-->')

" open and close a tag
noremap  <buffer> <Leader>< :call TagLine('<>', '', '</>')<CR>^a
inoremap <buffer> <Leader>< <C-\><C-O>:call ITagLine('<', '', '></>')<CR>
vnoremap <buffer> <Leader>< <ESC>:call TagBlock('<>', '', '</>')<CR>'<f<a
