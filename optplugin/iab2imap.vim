" Vim optional plugin file
" Hugo Raguet 2020
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Usage:        Convert all insert mode abbreviations into insert mode mappings

redir => all_abbs | silent iab | redir END
let all_abbs = split(all_abbs, '\n', 1)[2:]
edit ~/.vim/optplugin/iab2imap_tmp.vim
for l_abb in all_abbs
    let s_abb = split(l_abb, ' ')
    execute "normal iimap " . s_abb[2] . " " . s_abb[2] . ""
    execute "iunabbrev " . s_abb[2]
endfor

execute "Bwq"

source ~/.vim/optplugin/iab2imap_tmp.vim

!rm ~/.vim/optplugin/iab2imap_tmp.vim
