" Vim programming plugin file
" Hugo Raguet 2020
" Language:	    Commenting in any language
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Usage:
" Source in programming language syntax files and define the buffer variables
" b:com_* according to needs.
" Use F1 for commenting and F2 for uncommenting;
"   in normal or insert mode, comment the current line,
"   in visual mode, comment the selected area.
" Functions MapKeyToTags and MapKeyToTagsAndTitle allows to map other keys
" especially useful for handling delimiters

if exists('b:did_programming')
  finish
endif
let b:did_programming = 1

setlocal textwidth=79

" NOTA: backslashed will be automatically escaped for begin and end tags,
" but not for separation, in order to allow \t or \n.
" default parameters for linux config files
let b:com_lin_beg='#'
let b:com_lin_sep=' '
let b:com_lin_end=''
let b:com_bck_beg='#'
let b:com_bck_sep=' '
let b:com_bck_end=''

""" source code comments
function Escapebackslash(pat)
    return substitute(a:pat, '\\', '\\\\', "g")
endfunction

" comment a line, separating beg and end tags from content with sep
function TagLine(beg, sep, end)
    let l:beg = Escapebackslash(a:beg)
    let l:end = Escapebackslash(a:end)
    let l:pos = getcharpos(".")
    let l:pat = '^\(\s*\)\(.*\)'
    let l:sub = '\1' . l:beg . a:sep . '\2' . (strlen(l:end) > 0 ? a:sep : "")
        \ . l:end
    call setline(line("."), substitute(getline("."), l:pat, l:sub,  ""))
    let l:pos[2] += strchars(a:beg) + strchars(a:sep)
    call setcharpos(".", l:pos)
endfunction

" open an inline comment, separating beg and end tags from content with
" sep, and placing the cursor at the right place
function ITagLine(beg, sep, end)
    let l:pos = getcharpos(".")
    let l:line = getline(".")
    call setline(line("."), (l:pos[2] > 1 ? l:line[0:l:pos[2] - 2] : "")
        \ . a:beg . a:sep . (strlen(a:end) > 0 ? a:sep : "") . a:end
        \ . l:line[l:pos[2] - 1:-1])
    let l:pos[2] += strchars(a:beg) + strchars(a:sep)
    call setcharpos(".", l:pos)
endfunction

" uncomment a line, removing beg and end tags with possible duplications
" of trailing or leading character and trailing or leading sep
function UntagLine(beg, sep, end)
    let l:pos = getcharpos(".")
    let l:begrep = Escapebackslash(a:beg) . Escapebackslash(a:beg[-1:]) . '\*'
    let l:seprep = (strlen(a:sep) > 0 ? a:sep . '\*' : "")
    let l:endrep = (strlen(a:end) > 0 ? Escapebackslash(a:end[0]) . '\*' : "")
        \. Escapebackslash(a:end)
    let l:pat = '\V' . l:begrep . l:seprep . '\(\.\{-\}\)' . l:seprep
        \ . l:endrep
    let l:sub = '\1'
    call setline(line("."), substitute(getline("."), l:pat, l:sub,  ""))
    let l:pos[2] -= strchars(a:beg) + strchars(a:sep)
    call setcharpos(".", l:pos)
endfunction

" comment a block defined by visual area, separating beg and end tags
" from content with sep
function TagBlock(beg, sep, end)
    let l:pos = getcharpos("'>")
    let l:pos[1] -= (line("'>") - line("'<") + 1)
    let l:beg = Escapebackslash(a:beg)
    let l:end = Escapebackslash(a:end)
    let l:idt = (a:sep == '\n' ? '\1' : "")
    let l:pat = '^\(\s*\)\(.*\)'
    let l:sub = '\1' . l:beg . a:sep . l:idt . '\2'
        \ . (strlen(l:end) > 0 ? a:sep : "") . l:idt . l:end
    let l:lines = getline("'<", "'>")
    let l:startcol = charcol("'<")
    let l:endcol = charcol("'>")
    let l:before = strcharpart(l:lines[0], 0, l:startcol - 1)
    let l:after = strcharpart(l:lines[-1], l:endcol)
    if line("'<") == line("'>")
        let l:lines[0] = strcharpart(l:lines[0], l:startcol - 1,
            \ l:endcol - l:startcol + 1)
    else
        let l:lines[0] = strcharpart(l:lines[0], l:startcol - 1)
        let l:lines[-1] = strcharpart(l:lines[-1], 0, l:endcol)
    endif
    let l:lines = join(l:lines, "\n")
    let l:lines = split(substitute(l:lines, l:pat, l:sub, ""), '\n')
    let l:lines[0] = l:before . l:lines[0]
    let l:lines[-1] = l:lines[-1] . l:after
    call deletebufline("%", line("'<"), line("'>"))
    call append(line("'<") - 1, l:lines)
    let l:pos[1] += len(lines) 
    let l:pos[2] += strchars(a:beg) + strchars(a:sep)
    call setcharpos(".", l:pos)
endfunction

" uncomment a block defined by visual area, removing beg and end tags
" with possible duplications of trailing or leading character and trailing or
" leading sep
function UntagBlock(beg, sep, end)
    let l:pos = getcharpos("'>")
    let l:pos[1] -= line("'>") - line("'<") + 1
    let l:begrep = Escapebackslash(a:beg) . Escapebackslash(a:beg[-1:]) . '\*'
    let l:seprep = (strlen(a:sep) > 0 ? a:sep . '\*' : "")
    let l:endrep = (strlen(a:end) > 0 ? Escapebackslash(a:end[0]) . '\*' : "")
        \. Escapebackslash(a:end)
    let l:pat = '\V\%(\^\s\*' . l:begrep . '\n\|' . l:begrep . '\)'
        \ . l:seprep . '\(\.\{-}\)' . l:seprep
        \ . '\%(\n\s\*' . l:endrep . '\|' . l:endrep . '\)'
    let l:sub = '\1'
    let l:lines = join(getline("'<", "'>"), "\n")
    let l:lines = split(substitute(l:lines, l:pat, l:sub, ""), '\n')
    call deletebufline("%", line("'<"), line("'>"))
    call append(line("'<") - 1, l:lines)
    let l:pos[1] += len(lines)
    let l:pos[2] -= strchars(a:beg) + strchars(a:sep)
    call setcharpos(".", l:pos)
endfunction

" comment or uncomment each line of a block defined by visual area,
" separating beg and end tags from content with sep, or removing beg and end
" tags with possible duplications of trailing or leading character and trailing
" or leading sep
function TagBlockLines(beg, sep, end, un)
    let l:pos = getcharpos("'<") " [bufnum, lnum, col, off]
    let l:pos[1] -= 1
    while l:pos[1] < getcharpos("'>")[1]
        let l:pos[1] += 1
        call setcharpos(".", l:pos)
        if a:un
            call UntagLine(a:beg, a:sep, a:end)
        else
            call TagLine(a:beg, a:sep, a:end)
        endif
    endwhile 
endfunction

" call TagBlock or UntagBlock if end tag is not empty,
" or call TagBlocksLines otherwise
function TagBlockOrLines(beg, sep, end, un)
    if strlen(a:end) > 0
        if a:un
            call UntagBlock(a:beg, a:sep, a:end)
        else
            call TagBlock(a:beg, a:sep, a:end)
        endif
    else
        call TagBlockLines(a:beg, a:sep, a:end, a:un)
    endif
endfunction

" fancy header: frame a block defined by visual area, duplicating trailing and
" leading characters of a:beg and a:end tags, separating them from content with
" a:sep, and centering the result
function TagFrame(beg, sep, end)
    let l:lines = getline(line("'<"), line("'>"))
    let l:contentwidth = max(map(copy(l:lines), {_, val -> strchars(val)}))
    let l:framewidth = strchars(a:beg) + 2*strchars(a:sep) + strchars(a:end)
        \ + l:contentwidth
    let l:padding = repeat(' ', (&textwidth - l:framewidth)/2)
    let l:endbeg = repeat(' ', strchars(a:beg) - 1) . a:beg[-1:]
    let l:begend = strlen(a:end) > 0 ? a:end[0] : " "
    let l:firstline = l:padding . a:beg
        \ . repeat(a:beg[-1:], l:contentwidth + 2*strchars(a:sep)) . l:begend
    let l:lastline = l:padding . l:endbeg
        \ . repeat(l:begend, l:contentwidth + 2*strchars(a:sep)) . a:end
    call map(l:lines, {_, val -> l:padding . l:endbeg . a:sep . val
        \ . repeat(' ', l:contentwidth - strchars(val)) . a:sep . l:begend})
    call deletebufline("%", line("'<"), line("'>"))
    call append(line("'<") - 1, [l:firstline] + l:lines + [l:lastline])
    call setcharpos(".", getcharpos("'>"))
endfunction

" use F1 for commenting and F2 for uncommenting;
" in normal or insert mode, comment the current line,
" in visual mode, comment the selected area
noremap  <silent><buffer> <F1> :call
    \ TagLine(b:com_lin_beg, b:com_lin_sep, b:com_lin_end)<CR>
inoremap <silent><buffer> <F1> <C-\><C-O>:call
    \ ITagLine(b:com_lin_beg, b:com_lin_sep, b:com_lin_end)<CR>
vnoremap <silent><buffer> <F1> <ESC>:call
    \ TagBlockOrLines(b:com_bck_beg, b:com_bck_sep, b:com_bck_end, v:false)<CR>
noremap  <silent><buffer> <F2> :call
    \ UntagLine(b:com_lin_beg, b:com_lin_sep, b:com_lin_end)<CR>
inoremap <silent><buffer> <F2> <C-\><C-O>:call
    \ UntagLine(b:com_lin_beg, b:com_lin_sep, b:com_lin_end)<CR>
vnoremap <silent><buffer> <F2> <ESC>:call
    \ TagBlockOrLines(b:com_bck_beg, b:com_bck_sep, b:com_bck_end, v:true)<CR>

" function for automatic mappings with <Leader> keys
" this is usefull beyond commenting, in particular for tag-based languages
function MapKeyToTags(key, beg, sep, end)
    "once for normal comment line
    execute "noremap <silent><buffer> <Leader>"  . a:key . " :call "
        \ . "TagLine('" . a:beg . "', '" .  a:sep . "', '" . a:end . "')<CR>"
    execute "inoremap <silent><buffer> <Leader>"  . a:key . ' <C-\><C-O>:call '
        \ . "ITagLine('" . a:beg . "', '" .  a:sep . "', '" . a:end . "')<CR>"
    execute "vnoremap <silent><buffer> <Leader>"  . a:key . " <ESC>:call "
        \ . "TagBlockOrLines('" . a:beg . "', '" .  a:sep . "', '" . a:end
        \ . "', v:false)<CR>"
    " with backspace for uncommenting
    execute "noremap <silent><buffer> <Leader><BS>" . a:key . " :call "
        \ . "UntagLine('" . a:beg . "', '" . a:sep . "', '" . a:end . "')<CR>"
    execute "inoremap <silent><buffer> <Leader><BS>" . a:key
        \ . ' <C-\><C-O>:call UntagLine(' . "'" . a:beg . "', '" . a:sep
        \ . "', '" . a:end . "')<CR>"
    execute "vnoremap <silent><buffer> <Leader><BS>" . a:key . " <ESC>:call "
        \ . "TagBlockOrLines('" . a:beg . "', '" . a:sep . "', '" . a:end
        \ . "', v:true)<CR>"
endfunction

function MapKeyToTagsAndTitles(key, beg, sep, end)
    call MapKeyToTags(a:key, a:beg, a:sep, a:end)
    "two leaders for header
    let l:beg2 = a:beg . a:beg[-1:] . a:beg[-1:]
    let l:end2 = strlen(a:end) > 0 ? a:end[0] . a:end[0] . a:end : ""
    let l:sep2 = a:sep . a:sep
    execute "noremap <silent><buffer> <Leader><Leader>"  . a:key . " :call "
        \ . "TagLine('" . l:beg2 . "', '" .  l:sep2 . "', '" . l:end2
        \ . "')<CR>"
    execute "inoremap <silent><buffer> <Leader><Leader>"  . a:key
        \ . ' <C-\><C-O>:call ITagLine(' . "'" . l:beg2 . "', '" .  l:sep2
        \ . "', '" . l:end2 . "')<CR>"
    execute "vnoremap <silent><buffer> <Leader><Leader>"  . a:key
        \ . " <ESC>:call TagBlockOrLines('" . l:beg2 . "', '" . l:sep2 . "', '"
        \ . l:end2 . "', v:false)<CR>"
    "three leaders for frame
    execute "vnoremap <silent><buffer> <Leader><Leader><Leader>" . a:key
        \ . " <ESC>:call TagFrame('" . a:beg . "', '" . a:sep . "', '" . a:end
        \ . "')<CR>"
endfunction

" common delimiters
call MapKeyToTags('(', '(', '', ')')
call MapKeyToTags(')', '(', '', ')')
call MapKeyToTags('{', '{', '', '}')
call MapKeyToTags('}', '{', '', '}')
call MapKeyToTags('[', '[', '', ']')
call MapKeyToTags(']', '[', '', ']')
" first single quote does not need to be escaped by doubling
call MapKeyToTags("'", "''", '', "''")
call MapKeyToTags('"', '"', '', '"')
