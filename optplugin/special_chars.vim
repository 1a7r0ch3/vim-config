" Vim optional plugin file
" Hugo Raguet 2020
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Usage:        Type mathematical and other symbols

" inoremap <C-Space> <C-]>
" in non-gui mode, <C-Space> is interpreted as <C-@>
" see https://stackoverflow.com/a/12344382
imap <C-@> <C-Space>

" greek letters (let free those looking exactly like latin)
" inoremap <C-Space>A Α
" inoremap <C-Space>B Β
inoremap <C-Space>G Γ
inoremap <C-Space>D Δ
" inoremap <C-Space>E Ε
" inoremap <C-Space>Z Ζ
" inoremap <C-Space>Y Η
inoremap <C-Space>H Θ
" inoremap <C-Space>I Ι
" inoremap <C-Space>K Κ
inoremap <C-Space>L Λ
" inoremap <C-Space>M Μ
" inoremap <C-Space>N Ν
inoremap <C-Space>C Ξ
" inoremap <C-Space>O Ο
inoremap <C-Space>P Π
" inoremap <C-Space>R Ρ
inoremap <C-Space>S Σ
" inoremap <C-Space>T Τ
" inoremap <C-Space>U Υ
inoremap <C-Space>F Φ
inoremap <C-Space>X Χ
inoremap <C-Space>Q Ψ
inoremap <C-Space>W Ω
inoremap <C-Space>a α
inoremap <C-Space>b β
inoremap <C-Space>g γ
inoremap <C-Space>d δ
inoremap <C-Space>e ε
inoremap <C-Space>z ζ
inoremap <C-Space>y η
inoremap <C-Space>h θ
inoremap <C-Space>i ι
inoremap <C-Space>k κ
inoremap <C-Space>l λ
inoremap <C-Space>m μ
inoremap <C-Space>n ν
inoremap <C-Space>c ξ
" inoremap <C-Space>o ο
inoremap <C-Space>p π
inoremap <C-Space>r ρ
inoremap <C-Space>s σ
inoremap <C-Space>t τ
inoremap <C-Space>u υ
inoremap <C-Space>f φ
inoremap <C-Space>x χ
inoremap <C-Space>q ψ
inoremap <C-Space>w ω

" arrows
inoremap <C-Space><Left>  ←
inoremap <C-Space><Up>    ↑
inoremap <C-Space><Right> →
inoremap <C-Space><Down>  ↓

" mathematical symbols
inoremap <C-Space>=>  ⇒
inoremap <C-Space>==  ⇔
inoremap <C-Space>->  ↦
inoremap <C-Space>A   ∀
inoremap <C-Space>E   ∃
inoremap <C-Space>0   ∅
inoremap <C-Space>{   ∈
inoremap <C-Space>}   ∋
inoremap <C-Space>!   ∉ 
inoremap <C-Space>%   ∏
inoremap <C-Space>+   ∑
inoremap <C-Space>I   ∫
inoremap <C-Space>-   −
inoremap <C-Space>*   ×
inoremap <C-Space>@   ∗
inoremap <C-Space>°   ∘ 
inoremap <C-Space>.   ⋅
inoremap <C-Space>oc  ∝
inoremap <C-Space>oo  ∞
inoremap <C-Space>U   ∪
inoremap <C-Space>/\  ∩ 
inoremap <C-Space>\/  ∇
inoremap <C-Space>\d  ∂
inoremap <C-Space>/=  ≠ 
inoremap <C-Space>~   ≈
inoremap <C-Space><=  ≤
inoremap <C-Space>>=  ≥
inoremap <C-Space><<  ≪ 
inoremap <C-Space>>>  ≫
inoremap <C-Space>(   ⊂ 
inoremap <C-Space>)   ⊃
inoremap <C-Space><> ⟨⟩<ESC>hi
inoremap <C-Space><Bar> ││<ESC>i
inoremap <C-Space>#   ║║<ESC>i
inoremap <C-Space>[   ⌊⌋<ESC>i
inoremap <C-Space>]   ⌈⌉<ESC>i
inoremap <C-Space>/H  ℍ
inoremap <C-Space>/C  ℂ
inoremap <C-Space>N   ℕ
inoremap <C-Space>/Q  ℚ
inoremap <C-Space>R   ℝ
inoremap <C-Space>Z   ℤ
inoremap <C-Space>&   ℓ
inoremap <C-Space>V   √{}<ESC>i
inoremap <C-Space>v   ↖{→}

" for use with Tex and jQmath
inoremap <C-Space>_  _{}<ESC>i
inoremap <C-Space>^  ^{}<ESC>i
inoremap <C-Space><PageUp>    ↖{}<ESC>i
inoremap <C-Space><PageDown>  ↙{}<ESC>i
inoremap <C-Space>T { \table , ; ,
inoremap <C-Space>M ( \table , ; , )

" for fun
inoreabbr box\ ┌───┬───┐<CR>│   │   │<CR>├───┼───┤<CR>│   │   │<CR>└───┴───┘<CR>
inoreabbr mem\ ---┌─┬─┬─┬─┬─┬─┬─┬─┐---<CR>   │       │       │   <CR>---└─┴─┴─┴─┴─┴─┴─┴─┘---<CR>
