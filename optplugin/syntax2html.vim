" Vim optional plugin file
" Hugo Raguet 2020
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Usage:        Convert Vim syntax coloring into HTML, using specified tags
"               Once sourced, many keys and moves triggers the conversions,
"               allowing to convert on the fly while typing

if exists('g:loaded_syntax2html') && g:loaded_syntax2html
  finish
endif
let g:loaded_syntax2html = 1

" abbreviations used for smaller automatic HTML files
let hi_subs = [
    \ "Comment:M", "Constant:C", "Identifier:D", "Statement:E",
    \ "PreProc:R", "Type:T", "Operator:O", "Function:F", "LineNr:L",
    \ "Special:X",
    \ "pseudoComment:pM", "pseudoConstant:pC", "pseudoKeyword:pK",
    \ "pseudoOperator:pO", "pseudoFunction:pF", "pseudoLineNr:pL",
\ ]

" recopy syntax groups into abbreviations, and create links
for hi_sub in hi_subs
    let hi_grp =  hi_sub[:-3]
    let hi_abb =  hi_sub[-1:]
    redir => hi_cnf | execute "silent hi " . hi_grp | redir END
    let hi_cnf = split(hi_cnf, 'xxx')[1]
    if stridx(hi_cnf, "links") == -1
        execute "hi " . hi_abb . " " . hi_cnf
        execute "hi! link " . hi_grp . " " . hi_abb
    endif
endfor

let g:term = "cterm"
let g:HtmlSpace = ' '
let g:LeadingSpace = ' '
let g:HtmlEndline = ''
" let g:number_lines = &number
let g:line_style_name = synIDattr(synIDtrans(hlID('LineNr')), "name", g:term)

" write on a local file
let g:dist_ip = ""
" let g:html_file_base = "/home/latroche/Teaching/Remote/Terminal/content"
" let g:local_watch_file = "/home/latroche/Teaching/Remote/Terminal/watch.dat"
let g:html_file_base = "/home/latroche/Teaching/Remote/STerminal/content"
let g:local_watch_file = "/home/latroche/Teaching/Remote/STerminal/watch.dat"

" write on a distant server with netcat
" let g:html_file = expand("%").".html"
" let g:dist_ip = "192.168.1.101"
" let g:dist_port_base = "4444"

" mechanism for two different files
bufdo b:html_file_num = "2" 
let b:html_file_num = "1"

if has("autocmd")
    autocmd BufReadPost * if !exists('b:html_file_num') |
        \ let b:html_file_num = "2" | endif
    " organise information by lines for easier references
    autocmd BufReadPost * setlocal number
    " leave space for numbers and ensure two-column html display
    autocmd BufReadPost * setlocal textwidth=74 
endif

command LinkHtmlContent1 let b:html_file_num = "1"
command LinkHtmlContent2 let b:html_file_num = "2"

function! ExportToHTML(start_line, end_line)

    let l:lnum = a:start_line
    if l:lnum < 1 || l:lnum > line("$")
      let l:lnum = 1
    endif
    let l:end = a:end_line
    if l:end < l:lnum || l:end > line("$")
      let l:end = line("$")
    endif

    " if g:number_lines
        let l:margin = strlen(l:end) + 1
    " else
        " let l:margin = 0
    " endif

    let l:lines = []

    while l:lnum <= l:end

      let l:new = ""

      let l:line = getline(l:lnum)
      let l:len = strlen(l:line)

      " Start the line with the line number.
      " if g:number_lines
        let l:lnr = repeat(' ', l:margin - 1 - strlen(l:lnum)) . l:lnum . ' '
        let l:new = l:new . "<" . g:line_style_name . ">" . l:lnr
                        \ . "</" . g:line_style_name . ">"
      " endif

      " Loop over each character in the line
      let l:col = 1

      while l:col <= l:len
        let l:startcol = l:col " The start column for processing text

        let l:syn_id = synID(l:lnum, l:col, 1)
        let l:col = l:col + 1
        " Speed loop (it's small - that's the trick)
        " Go along till we find a change in synID
        while l:col <= l:len && l:syn_id == synID(l:lnum, l:col, 1)
            let l:col = l:col + 1
        endwhile
        let l:text = strpart(l:line, l:startcol - 1, l:col - l:startcol)
        " let l:text = strtrans(l:text)
        " Replace the reserved html characters
        let l:text = substitute(l:text, '&', '\&amp;',  'g')
        let l:text = substitute(l:text, '<', '\&lt;',   'g')
        let l:text = substitute(l:text, '>', '\&gt;',   'g')
        let l:text = substitute(l:text, '"', '\&quot;', 'g')

        if l:syn_id == 0
            let l:new = l:new . l:text
        else
            let l:style_name = synIDattr(synIDtrans(l:syn_id), "name", g:term)
            let l:new = l:new . "<" . l:style_name . ">" . l:text
                            \ . "</" . l:style_name . ">"
        endif

      endwhile
      call extend(l:lines, split(l:new . g:HtmlEndline, '\n', 1))
      let l:lnum = l:lnum + 1
    endwhile

    if g:dist_ip == "" " write results locally
        let l:html_file = g:html_file_base . b:html_file_num . ".html" 
        call writefile(l:lines, l:html_file, "s")
        " signal that a file has been modified
        call writefile([b:html_file_num], g:local_watch_file, "s")
    else " send to distant server
        let l:dist_address = "http://" . g:dist_ip . " " . g:dist_port_base
            \ . b:html_file_num
        call writefile(l:lines, l:html_file, "s")
        call system("nc -N " . b:distant_address ." < " . b:html_file)
    endif
    
endfunction

command ExportVisibleToHTML call ExportToHTML(line("w0"),line("w$"))

" <space> will be remapped below, so insertion abbreviations will not work;
" convert them into insertion mapping
source ~/.vim/optplugin/iab2imap.vim

inoremap <F5>     <C-\><C-O>:ExportVisibleToHTML<CR>
noremap  <F5>          :ExportVisibleToHTML<CR>
inoremap <Return> <CR><C-\><C-O>:ExportVisibleToHTML<CR>
inoremap <space>  <space><C-\><C-O>:ExportVisibleToHTML<CR>
noremap  dd       dd:ExportVisibleToHTML<CR>
noremap  o        o<C-\><C-O>:ExportVisibleToHTML<CR>
noremap  p        p:ExportVisibleToHTML<CR>
noremap  P        P:ExportVisibleToHTML<CR>
noremap  <C-D>    <C-D>:ExportVisibleToHTML<CR>
noremap  <C-U>    <C-U>:ExportVisibleToHTML<CR>
noremap  <C-E>    <C-E>:ExportVisibleToHTML<CR>
noremap  <C-Y>    <C-Y>:ExportVisibleToHTML<CR>
noremap  gg       gg:ExportVisibleToHTML<CR>
noremap  G        G:ExportVisibleToHTML<CR>
noremap  {        {:ExportVisibleToHTML<CR>
noremap  }        }:ExportVisibleToHTML<CR>
" inoremap <ESC>    <ESC>:ExportVisibleToHTML<CR>
""  mapping directly <ESC> breaks functions keys
""  so we only map explicit special cases instead
" the move commands must be put at the end of the macro because of the special
" but very frequent case of l when at the end of a line : since the cursor
" cannot go right, putting l before the call would raise an error and stop the
" macro, see http://vim.1045645.n5.nabble.com/Bug-l-breaks-macro-when-run-at-the-end-of-line-td1210855.html
inoremap <ESC>l <ESC>:ExportVisibleToHTML<CR>l
inoremap <ESC>k <ESC>:ExportVisibleToHTML<CR>k
inoremap <ESC>j <ESC>:ExportVisibleToHTML<CR>j
inoremap <ESC>h <ESC>:ExportVisibleToHTML<CR>h
inoremap <ESC>u <ESC>:ExportVisibleToHTML<CR>u
inoremap <ESC>p <ESC>:ExportVisibleToHTML<CR>p
inoremap <ESC>o <ESC>:ExportVisibleToHTML<CR>o
inoremap <ESC>A <ESC>:ExportVisibleToHTML<CR>A
