" Vim plugin file
" Hugo Raguet 2020
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Usage:
"   On xterm virtual terminal, alt+key combination does not send the META like
"   in other terminal (e.g. urxvt); the following emulate this behavior, useful
"   for leaving insert mode without reaching the ESC key.
" 
" get these characters sequences corresonding to a key combination with <C-S-v>
" in insertion mode 


inoremap [27;3;108~ <ESC>l
noremap  [27;3;108~ l
inoremap [27;3;107~ <ESC>k
noremap  [27;3;107~ k
inoremap [27;3;106~ <ESC>j
noremap  [27;3;106~ j
inoremap [27;3;104~ <ESC>h
noremap  [27;3;104~ h
inoremap [27;3;117~ <ESC>u
noremap  [27;3;117~ u
inoremap [27;3;112~ <ESC>p
noremap  [27;3;112~ p
inoremap [27;3;111~ <ESC>o
noremap  [27;3;111~ o
inoremap [27;3;98~  <ESC>b
noremap  [27;3;98~  b
inoremap [27;3;101~ <ESC>e
noremap  [27;3;101~ e
inoremap [27;3;102~ <ESC>f
noremap  [27;3;102~ f
inoremap [27;4;70~  <ESC>F
noremap  [27;4;70~  F
inoremap [27;3;116~ <ESC>t
noremap  [27;3;116~ t
inoremap [27;4;84~  <ESC>T
noremap  [27;4;84~  T
inoremap [27;4;65~  <ESC>A
noremap  [27;4;65~  A
inoremap [27;4;47~  <ESC>/
noremap  [27;4;47~  /
inoremap [27;4;63~  <ESC>?
noremap  [27;4;63~  ?
inoremap [27;3;58~  <ESC>:
noremap  [27;3;58~  :
inoremap [27;3;110~ <ESC>n
noremap  [27;3;110~ n
inoremap [27;4;78~  <ESC>N
noremap  [27;4;78~  N
inoremap [27;4;48~  <ESC>0
noremap  [27;4;48~  0
inoremap [27;3;94~  <ESC>^
noremap  [27;3;94~  ^
