" Vim syntax file
" Language:     Pseudocode (french)
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

if exists("b:current_syntax")
  finish
endif

syn keyword pseudoKeyword	réf
syn keyword pseudoKeyword	et ou non
syn keyword pseudoKeyword	Si alors sinon
syn keyword pseudoKeyword	Pour parcourant
syn keyword pseudoKeyword	répéter Répéter
syn keyword pseudoKeyword	Algorithme selon
syn keyword pseudoKeyword	exception terminer
syn keyword pseudoKeyword	nélém

syn match pseudoKeyword     "sinon si"
syn match pseudoKeyword     "Tant que"
syn match pseudoKeyword     "tant que"
syn match pseudoKeyword	    ","
syn match pseudoKeyword	    "("
syn match pseudoKeyword	    ")"
syn match pseudoKeyword	    "\."
syn match pseudoKeyword	    ":"

syn match pseudoNumber      "\<\d\+"
syn match pseudoNumber      "\<\d\+,\d\+"

syn match pseudoEmpty       "∅"

syn match pseudoOperator	"[+-]"
syn match pseudoOperator	"[=≠]"
syn match pseudoOperator	"[←→]"
syn match pseudoOperator	"[≥≤]"
syn match pseudoOperator	"[><]"
syn match pseudoOperator	"[/\\]"
syn match pseudoOperator	"[÷×]"
syn match pseudoOperator	"[—]"

syn match pseudoComment     "#.*$"

syn region pseudoString     start='“' end='”'
syn region pseudoChar       start='‘' end='’'

syn match pseudoFunction "\<\h\I*\>\(\s\|\n\)*("me=e-1

syn keyword pseudoBoolean   vrai faux

if &background ==# 'dark'
    hi  pseudoComment   term=italic cterm=italic ctermfg=188 guifg=#d7d7d7
    hi  pseudoConstant  ctermfg=225 guifg=#ffd7ff
    hi  pseudoKeyword   term=bold cterm=bold
    hi  pseudoOperator  term=bold cterm=bold ctermfg=230 guifg=#ffffd7
    hi  pseudoFunction  term=underline ctermfg=195 guifg=#d7ffff
    hi  pseudoLineNr    ctermfg=245 guifg=#8a8a8a
else
    hi  pseudoComment   term=italic cterm=italic ctermfg=240 guifg=#585858
    hi  pseudoConstant  ctermfg=88 guifg=#870000
    hi  pseudoKeyword   term=bold cterm=bold
    hi  pseudoOperator  term=bold cterm=bold ctermfg=130 guifg=#af5f00
    hi  pseudoFunction  term=underline ctermfg=18 guifg=#000087
    hi  pseudoLineNr    ctermfg=245 guifg=#8a8a8a
endif

hi def link pseudoBoolean       pseudoConstant
hi def link pseudoString        pseudoConstant
hi def link pseudoChar          pseudoConstant
hi def link pseudoNumber        pseudoConstant
hi def link pseudoEmpty         pseudoConstant
