" Vim syntax file
" Language:     Textboard (to be use with Remote Lecture software)
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

if exists("b:current_syntax")
  finish
endif

runtime syntax/html.vim

unlet b:current_syntax

syntax include @TextboardC syntax/c.vim
syntax region TextboardC
    \ matchgroup=Special
    \ start=+<code class="lang-c">+
    \ end=+</code>+he=s-1
    \ contains=@TextboardC keepend

unlet b:current_syntax

syntax include @TextboardPseudo syntax/pseudo.vim
syntax region TextboardPseudo
    \ matchgroup=Special
    \ start=+<code class="lang-pseudo">+
    \ end=+</code>+he=s-1
    \ contains=@TextboardPseudo keepend
