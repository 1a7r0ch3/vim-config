" Vimrc config file
" Language:	    Vim
" License:      GPL 3.0 see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt
" Hugo Raguet 2020

" Use Vim settings, rather then Vi settings;
" This must be first, because it changes other options as a side effect.
set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set history=50	" keep 50 lines of command line history
set ruler       " info about line/column/position of the cursor
set showcmd		" display incomplete commands
set incsearch	" do incremental searching

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
    set mouse=a
endif

" utf encoding
if has("multi_byte")
    set termencoding=utf-8
    set encoding=utf-8
    setglobal fileencoding=utf-8
    set fileencodings=utf-8,latin1
endif

""  appearance
" set background=dark "automagic on xterm and rxvt
syntax on
" Do not highlight the string we searched.
set nohlsearch
" Highlight the searched string, while typing.
set incsearch
" number of lines taken into account for parsing syntax
syntax sync minlines=200
syntax sync maxlines=1000

" discrete line numbers
hi  LineNr      ctermfg=245 guifg=#8a8a8a

" indentation
" set smarttab
set expandtab
set tabstop=4
set shiftwidth=4

" disable visual bell
set vb t_vb=

" default textwidth
set textwidth=0

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

" in case xterm does not sends escape with meta key
if $TERM=~"xterm"
    runtime optplugin/xterm_no_meta.vim
endif

" indentation (leave F1 and F2 for code source comments)
noremap  <F3> :<<CR>
noremap  <F4> :><CR>
inoremap <F3> <ESC>:<<CR>a
inoremap <F4> <ESC>:><CR>a
vnoremap <F3> <1v
vnoremap <F4> >1v

let mapleader = ''

" completion
inoremap <S-TAB> <C-X><C-P>
" the following does not work on xterm and urxvt, see https://stackoverflow.com/questions/2686766/mapping-c-tab-in-my-vimrc-fails-in-ubuntu
" inoremap <C-TAB> <C-X><C-L>

""  operations inside delimiters
" make $ a delimiter
onoremap <silent> i$ :<C-U>normal! T$vt$<CR>
onoremap <silent> a$ :<C-U>normal! F$vf$<CR>
vnoremap i$ T$ot$
vnoremap a$ F$of$

" select inside delimiters with space
function Getnextdelim(shift)
   let l = getline(".") 
   let c = col(".") + a:shift
   let s = 1
   while c <= col("$")
      for d in [")","]","}",'"',"'","`",">","$"]
           if l[c] == d
               return [d, s]
           endif
      endfor
      let c += 1
      let s += 1
   endwhile
   return ["w", 0]
endfunction

noremap  <Space>      :let [d,s]=Getnextdelim(0)<CR>:execute "normal vi" . d<CR>
vnoremap <Space>
    \ <ESC>:let [d,s]=Getnextdelim(1)<CR>:execute "normal " . s . "lvi" . d<CR>

" select words and paragraphs
noremap  <CR> viw
vnoremap <CR> ip

" select everything
map vA ggVG

" search for the visual selection
vnoremap * y/<C-R>"<CR>

"to be used with vim --cmd "let privatemode=1
if exists("privatemode") 
    let g:backup_dir = ""
    runtime optplugin/private.vim   
endif

if has("autocmd")

    " Enable file type detection.
    filetype plugin on
    command -buffer Ftconf execute "edit ~/.vim/ftplugin/" . &filetype . ".vim"

    augroup LastCursorPosition
        autocmd!
        " When editing a file, always jump to the last known cursor position.
        " Don't do it when the position is invalid or when inside an event
        " handler (happens when dropping a file on gvim).
        " Also don't do it when the mark is in the first line, that is the
        " default position when opening a file.
        autocmd BufReadPost *
            \ if line("'\"") > 1 && line("'\"") <= line("$") |
            \   execute "normal! g`\"" |
            \ endif
    augroup end "LastCursorPosition

    if !exists("g:backup_dir")
        let g:backup_dir = $HOME . "/.local/vim/backup/"
    endif

    if isdirectory(g:backup_dir)
        augroup VimBackup
            autocmd!

            " make one original backup, and backups at writing
            autocmd BufReadPost *
                \ if !filereadable(g:backup_dir . expand("%:t") . ".ori") |
                \   execute "write " . g:backup_dir . "%:t.ori" |
                \ endif
            autocmd BufDelete * silent!
                \ execute "!cp <afile> " . g:backup_dir . "<afile>:t.bak"
            autocmd VimLeave * bufdo execute "w! " . g:backup_dir . "%:t.bak"

        augroup end "VimBackup

        " replace the original backup with current buffer
        command Ori execute "w! " . g:backup_dir . "%:t.ori"

        " diff the current buffer with last backup
        command DiffBak execute "diffsplit " . g:backup . "%:t.bak"

    endif " isdirectory

    augroup SwapRecoveryAndDelete
        autocmd!
        autocmd SwapExists *
            \ let v:swapchoice = 'r' |
            \ let b:swapname = v:swapname |
            \ call delete(b:swapname)
    augroup end

    augroup CSVdisplay
        autocmd BufRead *.csv,*.tab,*.tsv exe "%ArrangeColumn" |
            \ exe "FillBlankCells" | exe "ReArrangeColumns"
        autocmd BufWritePost *.csv,*.tab,*.tsv %ArrangeColumn
        autocmd BufWritePre  *.csv,*.tab,*.tsv %UnArrangeColumn
    augroup end

endif " has("autocmd")

set nobackup
set nowritebackup

" rot13 to decode caesar's missive
" WARNING: do not inverse Chuck Norris's rot13 
command Rot13 normal ggg?G
command Rot5 :%s/\(\d\)/\=(submatch(0)+5)%10/g
command Rot18 execute "normal ggg?G" | :%s/\(\d\)/\=(submatch(0)+5)%10/g

" open a terminal with same current directory
command Term :silent !$TERMCMD&

""  Clipboard
" copy
vmap <C-y> "+y
" cut
map <C-c> "+c
" paste
map <C-p> "+p
imap <C-p> <ESC>"+pi

" clear a buffer
command Clear normal ggcG

" spell checkers
command Nospell setlocal nospell
command Spellen
    \ setlocal spell spelllang=en | syntax sync minlines=200 |
    \ syntax sync maxlines=1000 | syntax spell toplevel
command Spellus
    \ setlocal spell spelllang=en_us | syntax sync minlines=200 |
    \ syntax sync maxlines=1000 | syntax spell toplevel
command Spellfr
    \ setlocal spell spelllang=fr | syntax sync minlines=200 |
    \ syntax sync maxlines=1000 | syntax spell toplevel
command Spellde
    \ setlocal spell spelllang=de | syntax sync minlines=200 |
    \ syntax sync maxlines=1000 | syntax spell toplevel
command Spelles
    \ setlocal spell spelllang=es | syntax sync minlines=200 |
    \ syntax sync maxlines=1000 | syntax spell toplevel

" count words
command Countwords :%s/\(\w\+\)/\1/gn
" count characters
command Countchars :%s/\(.\)/\1/gn

" open vimrc
command Vimrc :edit ~/.vim/vimrc

" Allow saving of files as sudo when forgot using sudoedit.
command W execute ':silent write !sudo tee % >/dev/null' | :edit!

""  work with several buffers
set hidden
set switchbuf=useopen,usetab
" delete buffer (but not the windows on which it appears !)
command Bq :bp<bar>bd #
cnoreab bq <C-R>=(getcmdtype()==':' && getcmdpos()==1 ? 'Bq' : 'bq')<CR>
" write and delete buffer, windows/tab associated will be destroyed
command Bwd execute 'w' | :bd
cnoreab bwd <C-R>=(getcmdtype()==':' && getcmdpos()==1 ? 'Bwd' : 'bwd')<CR>
" write and delete buffer, keeping the windows/tab associated
command Bwq execute 'w' | :Bq
cnoreab bwq <C-R>=(getcmdtype()==':' && getcmdpos()==1 ? 'Bwq' : 'bwq')<CR>
" go to next/previous buffer.
map <C-H> :bprev<CR>
map <C-L> :bnext<CR>

" include subdirectories in search path 
let &path = "**," . &path

" for distant application that needs regular redrawing
" for some reason cannot use <F5> here but the corresponding terminal-specific
" issued command (get this in vim with <C-V><F5> in insertion mode)
command FrequentRedraw 
    \ execute "inoremap [15~ <ESC>:redraw!<CR>a" |
    \ execute "noremap [15~ :redraw!<CR>" |
    \ execute "inoremap <Return> <C-\><C-O>:redraw!<CR><CR>"

" enable typing greek letters and mathematical symbols
command SpecialChars runtime optplugin/special_chars.vim

" enable shortcuts for delimiters and comments
command Programming runtime optplugin/programming.vim
